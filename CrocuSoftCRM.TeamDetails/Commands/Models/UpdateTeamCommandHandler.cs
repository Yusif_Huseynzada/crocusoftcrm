﻿using CrocuSoftCRM.Domain.AggregatesModel.TeamAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Domain.Exceptions;
using CrocuSoftCRM.Identity.Queries;
using CrocuSoftCRM.Infrastructure.Commands;
using CrocuSoftCRM.Infrastructure.Idempotency;
using CrocuSoftCRM.NotFound.Exceptions;
using CrocuSoftCRM.TeamDetails.Commands.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace CrocuSoftCRM.TeamDetails.Commands
{
    public class UpdateTeamCommandHandler : IRequestHandler<UpdateTeamCommand, bool>
    {
        private readonly ITeamRepository _teamRepository;
        private readonly ITeamQueries _teamQueries;
        private readonly IUserRepository _userRepository;

        public UpdateTeamCommandHandler(ITeamRepository teamRepository, ITeamQueries teamQueries, IUserRepository userRepository)
        {
            _teamRepository = teamRepository;
            _teamQueries = teamQueries;
            _userRepository = userRepository;
        }

        public async Task<bool> Handle(UpdateTeamCommand request, CancellationToken cancellationToken)
        {
            var team = await _teamQueries.GetTeamEntityAsync(request.Id);

            var existingTeam = await _teamQueries.FindByNameAsync(request.TeamName);

            if (existingTeam != null)
            {
                throw new DomainException($"Team name '{request.TeamName}' already taken, please choose another name.");
            }

            team.SetDetails(request.TeamName);
            

            _teamRepository.UpdateAsync(team);
            await _teamRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return true;
        }

        public class UpdateTeamIdentifiedCommandHandler : IdentifiedCommandHandler<UpdateTeamCommand, bool>
        {
            public UpdateTeamIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
            {
            }

            protected override bool CreateResultForDuplicateRequest()
            {
                return true; // Tekrarlanan istekleri yok say
            }
        }
    }
}
