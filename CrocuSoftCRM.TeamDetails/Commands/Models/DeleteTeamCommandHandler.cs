﻿using CrocuSoftCRM.Domain.AggregatesModel.TeamAggregate;
using CrocuSoftCRM.Identity.Queries;
using CrocuSoftCRM.Infrastructure.Commands;
using CrocuSoftCRM.Infrastructure.Idempotency;
using CrocuSoftCRM.TeamDetails.Commands.Models;
using MediatR;

namespace CrocuSoftCRM.TeamDetails.Commands
{
    public class DeleteTeamCommandHandler : IRequestHandler<DeleteTeamCommand, bool>
    {
        private readonly ITeamRepository _teamRepository;
        private readonly ITeamQueries _teamQueries;

        public DeleteTeamCommandHandler(ITeamRepository teamRepository, ITeamQueries teamQueries)
        {
            _teamRepository = teamRepository ?? throw new ArgumentNullException(nameof(teamRepository));
            _teamQueries = teamQueries ?? throw new ArgumentNullException(nameof(teamQueries));
        }

        public async Task<bool> Handle(DeleteTeamCommand request, CancellationToken cancellationToken)
        {
            var team = await _teamQueries.GetTeamEntityAsync(request.Id);

            if (team.Users.Any())
            {
                return false;
            }

            _teamRepository.Delete(team);
            await _teamRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return true;
        }
    }
    public class DeleteTeamIdentifiedCommandHandler : IdentifiedCommandHandler<DeleteTeamCommand, bool>
    {
        public DeleteTeamIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }

    }
}
