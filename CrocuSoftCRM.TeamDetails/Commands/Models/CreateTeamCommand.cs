﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.TeamDetails.Commands.Models
{
    public class CreateTeamCommand : IRequest<bool>
    {
        public string TeamName { get; set; }
    }
}
