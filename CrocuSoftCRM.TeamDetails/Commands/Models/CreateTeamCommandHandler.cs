﻿using MediatR;
using CrocuSoftCRM.Domain.AggregatesModel.TeamAggregate;
using CrocuSoftCRM.SharedKernel.Infrastructure;
using CrocuSoftCRM.Infrastructure.Commands;
using CrocuSoftCRM.Infrastructure.Database;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using CrocuSoftCRM.Infrastructure.Idempotency;
using CrocuSoftCRM.TeamDetails.Commands.Models;
using CrocuSoftCRM.Infrastructure.Repositories;
using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;

namespace CrocuSoftCRM.TeamDetails.Commands
{
    public class CreateTeamCommandHandler : IRequestHandler<CreateTeamCommand, bool>
    {
        private readonly CrocuSoftCRMDbContext _context;
        private readonly ITeamRepository _teamRepository;
        private readonly IUserRepository _userRepository;

        public CreateTeamCommandHandler(CrocuSoftCRMDbContext context, ITeamRepository teamRepository, IUserRepository userRepository)
        {
            _context = context;
            _teamRepository = teamRepository;
            _userRepository = userRepository;
        }

        public async Task<bool> Handle(CreateTeamCommand request, CancellationToken cancellationToken)
        {
            var teamNameToLower = request.TeamName.ToLower();

            var existingTeam = await _context.Teams.FirstOrDefaultAsync(t => t.TeamName.ToLower() == teamNameToLower);

            if (existingTeam != null)
            {
                throw new ApplicationException($"The team '{request.TeamName}' already exists.");
            }

            var newTeam = new Team();
            newTeam.SetDetails(request.TeamName);

            await _teamRepository.AddAsync(newTeam);
            await _teamRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);

            return true;
        }
    }

    public class CreateTeamIdentifiedCommandHandler :
        IdentifiedCommandHandler<CreateTeamCommand, bool>
    {
        public CreateTeamIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true; // Ignore duplicate requests
        }
    }
}
