﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.TeamDetails.Commands.Models
{
    public class DeleteTeamCommand : IRequest<bool>
    {
        public int Id { get; set; }
    }
}
