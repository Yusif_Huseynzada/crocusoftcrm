﻿using Microsoft.AspNetCore.Authorization;

namespace CrocuSoftCRM.CustomAuthorizeLogic
{
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        public AuthorizeRolesAttribute(params string[] roles) : base()
        {
            Roles = string.Join(",", roles);
        }
    }
}
