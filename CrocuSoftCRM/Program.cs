using CrocuSoftCRM;
using CrocuSoftCRM.Extensions;
using CrocuSoftCRM.Infrastructure;
using CrocuSoftCRM.Infrastructure.Database;
using CrocuSoftCRM.Infrastructure.DBSeed;
using Microsoft.AspNetCore;
using Microsoft.Extensions.Options;
using Serilog;

namespace CrocusoftReport
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            Log.Logger = new LoggerConfiguration()
            .WriteTo.File("logs/log.txt", rollingInterval: RollingInterval.Day)
            .WriteTo.Seq("http://localhost:5341")
            .CreateLogger();

            host.MigrateDbContext<CrocuSoftCRMDbContext>(async (context, services) =>
            {
                var env = services.GetService<IWebHostEnvironment>();
                var settings = services.GetService<IOptions<CrocuSoftCRMSettings>>();
                var logger = services.GetService<ILogger<CrocuSoftCRMDbContextSeed>>();
                var seeder = new CrocuSoftCRMDbContextSeed();
                //await seeder.SeedAsync(context, env, settings, logger);
            });
            host.Run();
        }
        public static IWebHostBuilder CreateHostBuilder(string[] args) =>
        WebHost.CreateDefaultBuilder(args).ConfigureServices((hostContext, services) =>
        {

        }).UseSerilog().UseStartup<Startup>();
    }
}