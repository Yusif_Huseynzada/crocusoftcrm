﻿using CrocuSoftCRM.DailyReportDetails.Commands.Models;
using CrocuSoftCRM.Extensions;
using CrocuSoftCRM.Identity.Queries;
using CrocuSoftCRM.Identity.ViewModels;
using CrocuSoftCRM.Infrastructure.Constants;
using CrocuSoftCRM.ProjectDetails.Commands.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CrocuSoftCRM.Controllers
{
    public class DailyReportController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IDailyReportQueries _dailyReportQueries;

        public DailyReportController(IMediator mediator, IDailyReportQueries dailyReportQueries)
        {
            _mediator = mediator ?? throw new ArgumentException(nameof(mediator));
            _dailyReportQueries = dailyReportQueries ?? throw new ArgumentNullException(nameof(dailyReportQueries));
        }
        [HttpPost("create-dailyreport")]
        public async Task<IActionResult> CreateDailyReport([FromBody] CreateDailyReportCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<CreateDailyReportCommand, bool>(command, requestId);
            return NoContent();
        }

        [HttpPut("update-dailyreport")]
        public async Task<IActionResult> UpdateDailyReport([FromBody] UpdateDailyReportCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<UpdateDailyReportCommand, bool>(command, requestId);
            return NoContent();
        }

        [HttpDelete("delete-dailyreport")]
        public async Task<IActionResult> DeleteDailyReport([FromBody] DeleteDailyReportCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<DeleteDailyReportCommand, bool>(command, requestId);
            return NoContent();
        }

        [HttpGet("all")]
        public async Task<ActionResult<AllDailyReportDto>> GetAllProjects([FromQuery] DailyReportFilterDto loadMore)
        {
            var teams = await _dailyReportQueries.GetAllAsync(loadMore);
            return Ok(teams);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<GetDailyReportByIdDto>> GetDailyReportById(int id)
        {
            var team = await _dailyReportQueries.GetDailyReportById(id);
            return Ok(team);
        }
        [HttpGet("user-dailyreports")]
        public async Task<ActionResult<GetUserDailyReportByIdDto>> GetUserDailyReportsByUserId([FromQuery] UserDailyReportFilterDto loadMore)
        {
             var userDailyReport = await _dailyReportQueries.GetUserDailyReportsByUserId(loadMore);
             return Ok(userDailyReport);
        }
    }
}
