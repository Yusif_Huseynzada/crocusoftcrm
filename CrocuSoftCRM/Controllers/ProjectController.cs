﻿using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Extensions;
using CrocuSoftCRM.Identity.Queries;
using CrocuSoftCRM.Identity.ViewModels;
using CrocuSoftCRM.Infrastructure.Constants;
using CrocuSoftCRM.ProjectDetails.Commands.Models;
using CrocuSoftCRM.TeamDetails.Commands.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CrocuSoftCRM.Controllers
{
    [Route("project")]
    public class ProjectController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IProjectQueries _projectQueries;

        public ProjectController(IMediator mediator, IProjectQueries projectQueries)
        {
            _mediator = mediator ?? throw new ArgumentException(nameof(mediator));
            _projectQueries = projectQueries ?? throw new ArgumentNullException(nameof(projectQueries));
        }

        [HttpPost("create-project")]
        public async Task<IActionResult> CreateProject([FromBody] CreateProjectCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<CreateProjectCommand, bool>(command, requestId);
            return NoContent();
        }

        [HttpPut("update-project")]
        public async Task<IActionResult> UpdateProject([FromBody] UpdateProjectCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<UpdateProjectCommand, bool>(command, requestId);
            return NoContent();
        }

        [HttpGet("all")]
        public async Task<ActionResult<AllProjectDto>> GetAllProjects([FromQuery] ProjectFilterDto loadMore)
        {
            var teams = await _projectQueries.GetAllAsync(loadMore);
            return Ok(teams);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<GetProjectByIdDto>> GetProjectById(int id)
        {
            var team = await _projectQueries.GetProjectById(id);
            return Ok(team);
        }


    }
}
