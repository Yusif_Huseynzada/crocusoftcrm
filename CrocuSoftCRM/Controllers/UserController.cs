﻿using System;
using System.Threading.Tasks;
using CrocuSoftCRM.CustomAuthorizeLogic;
using CrocuSoftCRM.Extensions;
using CrocuSoftCRM.Identity.Queries;
using CrocuSoftCRM.Identity.ViewModels;
using CrocuSoftCRM.Infrastructure.Constants;
using CrocuSoftCRM.SharedKernel.Domain.Seedwork;
using CrocuSoftCRM.UserDetails.Commands;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CrocuSoftCRM.Controllers
{
    [Route("user")]
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IUserQueries _userQueries;

        public UserController(IMediator mediator, IUserQueries userQueries)
        {
            _mediator = mediator ?? throw new ArgumentException(nameof(mediator));
            _userQueries = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
        }


        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromQuery] RegisterUserCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<RegisterUserCommand, bool>(command, requestId);
           
            return NoContent();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPut("changePassword")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<ChangePasswordCommand, bool>(command, requestId);

            return Ok();
        }

        //[AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] UpdateUserCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<UpdateUserCommand, bool>(command, requestId);

            return Ok();
        }

        [AllowAnonymous]
        [HttpPut("forgotPassword")]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<ForgotPasswordCommand, bool>(command, requestId);

            return NoContent();
        }


        [HttpPost("validate")]
        public async Task<IActionResult> ValidateToken([FromBody] CheckTokenVerifyInputCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<CheckTokenVerifyInputCommand, bool>(command, requestId);

            return Ok();
        }

        //[AuthorizeRoles(CustomRole.SuperAdmin)]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] DeleteUserCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<DeleteUserCommand, bool>(command, requestId);

            return NoContent();
        }

        //[AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPatch("active")]
        public async Task<IActionResult> Active([FromBody] ActiveUserCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<ActiveUserCommand, bool>(command, requestId);

            return NoContent();
        }

        //[AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpGet("alladmin")]
        public async Task<AllAdminDto> GetAllAdminAsync(LoadMoreDto loadMore)
        {
            return await _userQueries.GetAllAdminAsync(loadMore);
        }

        [HttpGet("alluser")]
        public async Task<AllUserProfileDto> GetAllUserAsync(UserFilterDto loadMore)
        {
            return await _userQueries.GetAllUserAsync(loadMore);
        }

        //[AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpGet("role-admin&superadmin/{id}")]
        public async Task<AdminDto> GetAdminByIdAsync(int id)
        {
            return await _userQueries.GetAdminById(id);
        }

        [HttpGet("user/{id}")]
        public async Task<GetUserByIdDto> GetUserByIdAsync(int id)
        {
            return await _userQueries.GetUserById(id);
        }
    }
}
