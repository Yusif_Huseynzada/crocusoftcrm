﻿using CrocuSoftCRM.CustomAuthorizeLogic;
using CrocuSoftCRM.Domain.AggregatesModel.TeamAggregate;
using CrocuSoftCRM.Extensions;
using CrocuSoftCRM.Identity.Queries;
using CrocuSoftCRM.Identity.ViewModels;
using CrocuSoftCRM.Infrastructure.Constants;
using CrocuSoftCRM.SharedKernel.Domain.Seedwork;
using CrocuSoftCRM.TeamDetails.Commands.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CrocuSoftCRM.Controllers
{
    [Route("team")]
    public class TeamController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ITeamQueries _teamQueries;

        public TeamController(IMediator mediator, ITeamQueries teamQueries)
        {
            _mediator = mediator ?? throw new ArgumentException(nameof(mediator));
            _teamQueries = teamQueries ?? throw new ArgumentNullException(nameof(teamQueries));
        }
       
        
        [HttpPost("create-team")]
        public async Task<IActionResult> CreateTeam([FromBody] CreateTeamCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<CreateTeamCommand, bool>(command, requestId);
            return NoContent();
        }

        [HttpPut("update-team")]
        public async Task<IActionResult> UpdateTeam([FromBody] UpdateTeamCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<UpdateTeamCommand, bool>(command, requestId);
            return NoContent();
        }

        [HttpDelete("delete-team")]
        public async Task<IActionResult> DeleteTeam([FromBody] DeleteTeamCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<DeleteTeamCommand, bool>(command, requestId);
            return NoContent();
        }

        [HttpGet("all")]
        public async Task<ActionResult<AllTeamDto>> GetAllTeams([FromQuery] LoadMoreDto loadMore)
        {
            var teams = await _teamQueries.GetAllAsync(loadMore);
            return Ok(teams);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<GetTeamByIdDto>> GetTeamById(int id)
        {
            var team = await _teamQueries.GetTeamById(id);
            return Ok(team);
        }
    }
}
