﻿using CrocuSoftCRM.Identity.Queries;
using CrocuSoftCRM.Identity.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CrocuSoftCRM.Controllers
{
    [Route("role")]
    public class RoleController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IRoleQueries _roleQueries;

        public RoleController(IMediator mediator, IRoleQueries roleQueries)
        {
            _mediator = mediator ?? throw new ArgumentException(nameof(mediator));
            _roleQueries = roleQueries ?? throw new ArgumentNullException(nameof(roleQueries));
        }

        [HttpGet]
        public async Task<IEnumerable<RoleDto>> GetAllAsync()
        {
            return await _roleQueries.GetAllAsync();
        }
    }
}
