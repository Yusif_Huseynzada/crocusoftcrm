﻿using Autofac;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Identity;
using CrocuSoftCRM.Identity.Auth;
using CrocuSoftCRM.Infrastructure.Idempotency;
using CrocuSoftCRM.Infrastructure.Identity;
using CrocuSoftCRM.Infrastructure.Repositories;
using CrocuSoftCRM.UserDetails;
using CrocuSoftCRM.Domain.AggregatesModel.DailyReportAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.TeamAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;

namespace CrocuSoftCRM.Infrastructure.AutofacModules
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            AutofacHelper.RegisterCqrsTypes<ApplicationModule>(builder);
            AutofacHelper.RegisterCqrsTypes<IdentityModule>(builder);
            AutofacHelper.RegisterCqrsTypes<UserModule>(builder);
            AutofacHelper.RegisterCqrsTypes<TeamModule>(builder);
            AutofacHelper.RegisterCqrsTypes<DailyReportModule>(builder);
            AutofacHelper.RegisterCqrsTypes<ProjectModule>(builder);

            // Services

            // Repositories
            builder.RegisterType<ClaimsManager>()
                .As<IClaimsManager>()
                .InstancePerLifetimeScope();


            builder.RegisterType<RequestManager>()
                .As<IRequestManager>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserManager>()
                .As<IUserManager>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserRepository>()
                .As<IUserRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<DailyReportRepository>()
                .As<IDailyReportRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<TeamRepository>()
                .As<ITeamRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ProjectRepository>()
              .As<IProjectRepository>()
              .InstancePerLifetimeScope();

            // AutoMapper
            AutofacHelper.RegisterAutoMapperProfiles<ApplicationModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<IdentityModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<UserModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<TeamModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<DailyReportModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<ProjectModule>(builder); 


            builder.Register(ctx =>
            {
                var mapperConfiguration = new MapperConfiguration(cfg =>
                {
                    foreach (var profile in ctx.Resolve<IList<Profile>>()) cfg.AddProfile(profile);
                });
                return mapperConfiguration.CreateMapper();
            })
                .As<IMapper>()
                .SingleInstance();
        }
    }
}
