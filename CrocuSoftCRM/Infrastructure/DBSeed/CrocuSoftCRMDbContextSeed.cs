﻿using CrocuSoftCRM.Domain.AggregatesModel.RoleAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.RoleAggregate;
using CrocuSoftCRM.Infrastructure.Database;
using CrocuSoftCRM.SharedKernel.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Polly;
using Polly.Retry;
using System.Data.SqlClient;

namespace CrocuSoftCRM.Infrastructure.DBSeed
{
    public class CrocuSoftCRMDbContextSeed
    {
        public async Task SeedAsync(
            CrocuSoftCRMDbContext context,
            IWebHostEnvironment env,
            IOptions<CrocuSoftCRMSettings> settings,
            ILogger<CrocuSoftCRMDbContextSeed> logger)
        {
            var policy = CreatePolicy(logger, nameof(CrocuSoftCRMDbContextSeed));

            using (context)
            {
                await policy.ExecuteAsync(async () =>
                {
                    await context.SaveChangesAsync();
                    var systemUser = await context.Users.FirstOrDefaultAsync();
                    if (systemUser == null)
                    {
                        var superAdminRole = new Role();
                        superAdminRole.SetDetails(RoleParametr.SuperAdmin.Name, "Super Admin");
                        await context.Roles.AddAsync(superAdminRole);
                        var adminRole = new Role();
                        adminRole.SetDetails(RoleParametr.Admin.Name, "Admin");
                        await context.Roles.AddAsync(adminRole);
                        var humanResourceRole = new Role();
                        var headRole = new Role();
                        headRole.SetDetails(RoleParametr.Head.Name, "Head");
                        await context.Roles.AddAsync(headRole);
                        var userRole = new Role();
                        userRole.SetDetails(RoleParametr.Employee.Name, "User");
                        await context.Roles.AddAsync(userRole);
                        var merchantRole = new Role();
                        await context.SaveChangesAsync();
                        var user = new User ("marketing@crocusoft.com", "055-227-05-53", PasswordHasher.HashPassword("sdfsdfsdf@"), "SuperAdmin", "test");
                        user.SetRole(superAdminRole.Id);
                        await context.Users.AddAsync(user);
                        await context.SaveChangesAsync();
                    }
                });
            }
        }
        private AsyncRetryPolicy CreatePolicy(ILogger<CrocuSoftCRMDbContextSeed> logger, string prefix, int retries = 3)
        {
            return Policy.Handle<SqlException>().WaitAndRetryAsync(
                retries,
                retry => TimeSpan.FromSeconds(5),
                (exception, timeSpan, retry, ctx) =>
                {
                    logger.LogTrace(
                        $"[{prefix}] Exception {exception.GetType().Name} with message ${exception.Message} detected on attempt {retry} of {retries}");
                }
            );
        }
    }
}
