﻿using System.Linq.Expressions;

namespace CrocuSoftCRM.SharedKernel.Domain.Seedwork
{
    public interface IRepository<T> where T : IAggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }

        Task<T> AddAsync(T entity);

        Task<T> GetAsync(int id);
        public T Get(Expression<Func<T, bool>> exp, params string[] includes);

        T UpdateAsync(T entity);

        bool DeleteAsync(T entity);
    }
}
