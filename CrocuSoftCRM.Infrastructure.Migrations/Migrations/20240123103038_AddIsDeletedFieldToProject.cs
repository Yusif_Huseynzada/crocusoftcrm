﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CrocuSoftCRM.Infrastructure.Migrations.Migrations
{
    public partial class AddIsDeletedFieldToProject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Projects",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "DailyReports",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_DailyReports_UserId",
                table: "DailyReports",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_DailyReports_Users_UserId",
                table: "DailyReports",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DailyReports_Users_UserId",
                table: "DailyReports");

            migrationBuilder.DropIndex(
                name: "IX_DailyReports_UserId",
                table: "DailyReports");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "DailyReports");
        }
    }
}
