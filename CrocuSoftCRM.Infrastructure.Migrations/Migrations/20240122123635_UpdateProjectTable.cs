﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CrocuSoftCRM.Infrastructure.Migrations.Migrations
{
    public partial class UpdateProjectTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_DailyReports_DailyReportId",
                table: "Projects");

            migrationBuilder.DropIndex(
                name: "IX_Projects_DailyReportId",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "DailyReportId",
                table: "Projects");

            migrationBuilder.AddColumn<int>(
                name: "ProjectId",
                table: "DailyReports",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_DailyReports_ProjectId",
                table: "DailyReports",
                column: "ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_DailyReports_Projects_ProjectId",
                table: "DailyReports",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DailyReports_Projects_ProjectId",
                table: "DailyReports");

            migrationBuilder.DropIndex(
                name: "IX_DailyReports_ProjectId",
                table: "DailyReports");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "DailyReports");

            migrationBuilder.AddColumn<int>(
                name: "DailyReportId",
                table: "Projects",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Projects_DailyReportId",
                table: "Projects",
                column: "DailyReportId");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_DailyReports_DailyReportId",
                table: "Projects",
                column: "DailyReportId",
                principalTable: "DailyReports",
                principalColumn: "Id");
        }
    }
}
