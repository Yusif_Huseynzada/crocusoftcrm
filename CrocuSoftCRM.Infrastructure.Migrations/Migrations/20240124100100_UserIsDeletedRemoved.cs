﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CrocuSoftCRM.Infrastructure.Migrations.Migrations
{
    public partial class UserIsDeletedRemoved : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Users");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Users",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }
    }
}
