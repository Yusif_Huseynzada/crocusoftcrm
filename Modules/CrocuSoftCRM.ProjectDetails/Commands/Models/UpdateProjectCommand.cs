﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.ProjectDetails.Commands.Models
{
    public class UpdateProjectCommand : IRequest<bool>
    {
        public int Id { get; set; }
        public string ProjectName { get; set; }
        public List<int> UserIds { get; set; }
    }
}

