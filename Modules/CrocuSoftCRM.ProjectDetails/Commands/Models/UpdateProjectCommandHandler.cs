﻿using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Domain.Exceptions;
using CrocuSoftCRM.Identity.Queries;
using CrocuSoftCRM.Infrastructure.Commands;
using CrocuSoftCRM.Infrastructure.Database;
using CrocuSoftCRM.Infrastructure.Idempotency;
using CrocuSoftCRM.NotFound.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CrocuSoftCRM.ProjectDetails.Commands.Models
{
    public class UpdateProjectCommandHandler : IRequestHandler<UpdateProjectCommand, bool>
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IUserRepository _userRepository;
        private readonly CrocuSoftCRMDbContext _context;


        public UpdateProjectCommandHandler(IProjectRepository projectRepository, IUserRepository userRepository, CrocuSoftCRMDbContext context)
        {
            _projectRepository = projectRepository;
            _userRepository = userRepository;
            _context = context;
           
        }
        public async Task<bool> Handle(UpdateProjectCommand request, CancellationToken cancellationToken)
        {
            var project = await _projectRepository.FirstOrDefaultAsync(p => p.Id == request.Id);

            if (request.UserIds != null && request.UserIds.Any())
            {
                var existingProjects = await _context.UserProjects.Where(up => up.ProjectId == project.Id).ToListAsync();

                foreach (var existingProject in existingProjects)
                {
                    if (request.UserIds.Contains(existingProject.UserId))
                    {
                        _context.UserProjects.Remove(existingProject);
                    }
                }

                var users = await _context.Users.Where(u => request.UserIds.Contains(u.Id)).ToListAsync();

                foreach (var userId in request.UserIds)
                {
                    if (existingProjects.Any(ep => ep.UserId == userId))
                    {
                        continue;
                    }

                    var userProject = new UserProject(userId, project.Id);
                    _context.UserProjects.Add(userProject);
                }
            }

            project.SetDetails(request.ProjectName);

            _projectRepository.UpdateAsync(project);
            await _projectRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return true;
        }


        public class UpdateProjectIdentifiedCommandHandler : IdentifiedCommandHandler<UpdateProjectCommand, bool>
        {
            public UpdateProjectIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
            {
            }

            protected override bool CreateResultForDuplicateRequest()
            {
                return true; // Tekrarlanan istekleri yok say
            }
        }
    }
}
