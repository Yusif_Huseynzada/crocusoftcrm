﻿using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.TeamAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Identity.Queries;
using CrocuSoftCRM.Infrastructure.Commands;
using CrocuSoftCRM.Infrastructure.Database;
using CrocuSoftCRM.Infrastructure.Idempotency;
using CrocuSoftCRM.Infrastructure.Repositories;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.ProjectDetails.Commands.Models
{
    public class CreateProjectCommandHandler : IRequestHandler<CreateProjectCommand, bool>
    {
        private readonly CrocuSoftCRMDbContext _context;
        private readonly IProjectRepository _projectRepository;
        private readonly IUserQueries _userQueries;

        public CreateProjectCommandHandler(CrocuSoftCRMDbContext context, IProjectRepository projectRepository, IUserQueries userQuerires)
        {
            _context = context;
            _projectRepository = projectRepository;
            _userQueries = userQuerires;
        }

        public async Task<bool> Handle(CreateProjectCommand request, CancellationToken cancellationToken)
        {
            var projectNameToLower = request.ProjectName.ToLower();

            var existingProject = await _context.Projects.FirstOrDefaultAsync(t => t.ProjectName.ToLower() == projectNameToLower);

            if (existingProject != null)
            {
                throw new ApplicationException($"The project '{request.ProjectName}' already exists.");
            }

            var newProject = new Project();
            newProject.SetDetails(request.ProjectName);

          

            if (request.UserIds != null && request.UserIds.Any())
            {
                var users = await _userQueries.GetUsersByIdsAsync(request.UserIds);

                foreach (var user in users)
                {
                    newProject.AddUser(user);
                }
            }

            await _projectRepository.AddAsync(newProject);
            await _projectRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);

            return true;
        }
    }

    public class CreateTeamIdentifiedCommandHandler :
        IdentifiedCommandHandler<CreateProjectCommand, bool>
    {
        public CreateTeamIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true; // Ignore duplicate requests
        }
    }
}
