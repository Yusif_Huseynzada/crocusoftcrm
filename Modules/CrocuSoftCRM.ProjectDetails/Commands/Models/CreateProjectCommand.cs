﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.ProjectDetails.Commands.Models
{
    public class CreateProjectCommand : IRequest<bool>
    {
        public string ProjectName { get; set; }
        public List<int> UserIds { get; set; }
    }
}
