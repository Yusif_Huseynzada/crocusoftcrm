﻿using CrocuSoftCRM.Domain.AggregatesModel.DailyReportAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Infrastructure.Commands;
using CrocuSoftCRM.Infrastructure.Idempotency;
using CrocuSoftCRM.NotFound.Exceptions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.DailyReportDetails.Commands.Models
{
    public class CreateDailyReportCommandHandler : IRequestHandler<CreateDailyReportCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IDailyReportRepository _dailyReportRepository;

        public CreateDailyReportCommandHandler(IUserRepository userRepository, IProjectRepository projectRepository, IDailyReportRepository dailyReportRepository)
        {
            _userRepository = userRepository;
            _projectRepository = projectRepository;
            _dailyReportRepository = dailyReportRepository;
        }

        public async Task<bool> Handle(CreateDailyReportCommand request, CancellationToken cancellationToken)
        {
            var dailyReport = new DailyReport();
            dailyReport.SetDetails(request.Description);
            dailyReport.SetUser(request.UserId);
            dailyReport.SetProject(request.ProjectId);

            await _dailyReportRepository.AddAsync(dailyReport);
            await _dailyReportRepository.UnitOfWork.SaveChangesAsync();

            // Başarıyla tamamlandı
            return true;
        }
        public class CreateDailyReportIdentifiedCommandHandler :
        IdentifiedCommandHandler<CreateDailyReportCommand, bool>
        {
            public CreateDailyReportIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
            {
            }

            protected override bool CreateResultForDuplicateRequest()
            {
                return true; // Ignore duplicate requests
            }
        }
    }
}
