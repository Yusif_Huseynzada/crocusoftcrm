﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.DailyReportDetails.Commands.Models
{
    public class UpdateDailyReportCommand : IRequest<bool>
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
