﻿using CrocuSoftCRM.Domain.AggregatesModel.DailyReportAggregate;
using CrocuSoftCRM.Identity.Queries;
using CrocuSoftCRM.Infrastructure.Commands;
using CrocuSoftCRM.Infrastructure.Idempotency;
using MediatR;

namespace CrocuSoftCRM.DailyReportDetails.Commands.Models
{
    public class UpdateDailyReportCommandHandler : IRequestHandler<UpdateDailyReportCommand, bool>
    {
        private readonly IDailyReportRepository _dailyReportRepository;
        private readonly IDailyReportQueries _dailyReportQueries;

        public UpdateDailyReportCommandHandler(
            IDailyReportRepository dailyReportRepository,
            IDailyReportQueries dailyReportQueries
           )
        {
            _dailyReportRepository = dailyReportRepository;
            _dailyReportQueries = dailyReportQueries;
        }

        public async Task<bool> Handle(UpdateDailyReportCommand request, CancellationToken cancellationToken)
        {
            var dailyReport = await _dailyReportQueries.FindAsync(request.Id);

            if (dailyReport.CreatedAt.Date != DateTime.Now.Date)
            {
                throw new InvalidOperationException("Günlük rapor sadece oluşturulduğu gün güncellenebilir.");
                return false;
            }

            dailyReport.SetDetails(request.Description);

            _dailyReportRepository.UpdateAsync(dailyReport);
            await _dailyReportRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return true;
        }

    }

    public class UpdateDailyReportIdentifiedCommandHandler : IdentifiedCommandHandler<UpdateDailyReportCommand, bool>
    {
        public UpdateDailyReportIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true; // Ignore duplicate requests
        }
    }
}
