﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.DailyReportDetails.Commands.Models
{
    public class UpdateDailyReportCommandValidator : AbstractValidator<UpdateDailyReportCommand>
    {
        public UpdateDailyReportCommandValidator() : base()
        {
            RuleFor(command => command.Id).NotNull();
            RuleFor(command => command.Description).NotNull();
        }
    }
}
