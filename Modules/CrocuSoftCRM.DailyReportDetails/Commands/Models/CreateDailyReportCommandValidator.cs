﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.DailyReportDetails.Commands.Models
{
    public class CreateDailyReportCommandValidator : AbstractValidator<CreateDailyReportCommand>
    {
        public CreateDailyReportCommandValidator() : base()
        {
            RuleFor(command => command.Description).NotNull();
            RuleFor(command => command.UserId).NotNull().GreaterThan(0);
            RuleFor(command => command.ProjectId).NotNull().GreaterThan(0);
        }
    }
}
