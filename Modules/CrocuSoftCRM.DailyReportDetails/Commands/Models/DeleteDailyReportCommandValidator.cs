﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.DailyReportDetails.Commands.Models
{
    public class DeleteDailyReportCommandValidator : AbstractValidator<DeleteDailyReportCommand>
    {
        public DeleteDailyReportCommandValidator() : base()
        {
            RuleFor(command => command.Id).NotNull();
        }
    }
}
