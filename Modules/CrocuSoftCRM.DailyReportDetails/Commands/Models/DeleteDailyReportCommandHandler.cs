﻿using CrocuSoftCRM.Domain.AggregatesModel.DailyReportAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Identity.Queries;
using CrocuSoftCRM.Infrastructure.Commands;
using CrocuSoftCRM.Infrastructure.Database;
using CrocuSoftCRM.Infrastructure.Idempotency;
using CrocuSoftCRM.Infrastructure.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.DailyReportDetails.Commands.Models
{
    public class DeleteDailyReportCommandHandler : IRequestHandler<DeleteDailyReportCommand, bool>
    {
        private readonly IDailyReportRepository _dailyReportRepository;
        private readonly IDailyReportQueries _dailyReportQueries;

        public DeleteDailyReportCommandHandler(IDailyReportRepository dailyReportRepository, IDailyReportQueries dailyReportQueries)
        {
            _dailyReportRepository = dailyReportRepository ?? throw new ArgumentNullException(nameof(dailyReportRepository));
            _dailyReportQueries = dailyReportQueries ?? throw new ArgumentNullException(nameof(dailyReportQueries));
        }

        public async Task<bool> Handle(DeleteDailyReportCommand request, CancellationToken cancellationToken)
        {
            var dailyReport = await _dailyReportQueries.GetDailyReportEntityAsync(request.Id);
            
            _dailyReportRepository.Delete(dailyReport);
            await _dailyReportRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return true;
        }

        public class DeleteDailyReportIdentifiedCommandHandler : IdentifiedCommandHandler<DeleteDailyReportCommand, bool>
        {
            public DeleteDailyReportIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
            {
            }

            protected override bool CreateResultForDuplicateRequest()
            {
                return true;
            }
        }
    }
}
