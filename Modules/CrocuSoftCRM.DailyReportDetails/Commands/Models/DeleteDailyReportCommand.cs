﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.DailyReportDetails.Commands.Models
{
    public class DeleteDailyReportCommand : IRequest<bool>
    {
        public int Id { get; set; }
    }
}
