﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.DailyReportDetails.Commands.Models
{
    public class CreateDailyReportCommand : IRequest<bool>
    {
        public string Description { get; set; }
        public int UserId { get; set; }
        public int ProjectId { get; set; }
    }
}
