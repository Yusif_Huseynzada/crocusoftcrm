﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrocuSoftCRM.Identity.ViewModels
{
    public class AllAdminDto
    {
        public IEnumerable<AdminDto> Data { get; set; }
        public int TotalCount { get; set; }
    }
}
