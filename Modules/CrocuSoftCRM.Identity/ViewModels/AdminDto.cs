﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrocuSoftCRM.Identity.ViewModels
{
    public class AdminDto
    {
        public int Id { get; set; }
        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public string Email { get; private set; }

        public string Phone { get; private set; }

        public bool IsActive { get; private set; }
    }
}
