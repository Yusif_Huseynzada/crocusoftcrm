﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Identity.ViewModels
{
    public class DailyReportDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public GetDailyReportUserDto User { get; set; }
        public ProjectDto Project { get; set; }
       
    }
}
