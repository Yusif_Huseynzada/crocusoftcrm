﻿namespace CrocuSoftCRM.Identity.ViewModels
{
    public class UserProfileDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public RoleDto Role { get; set; }
        public TeamDto Team { get; set; }
       
    }
}
