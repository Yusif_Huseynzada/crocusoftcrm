﻿using CrocuSoftCRM.Infrastructure.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Identity.ViewModels
{
    public class UserFilterDto
    {
        public LoadMoreDto? Filter { get; set; }
        public List<string>? TeamNames { get; set; }
        public List<string>? ProjectNames { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
    }
}
