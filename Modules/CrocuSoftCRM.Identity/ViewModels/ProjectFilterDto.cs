﻿using CrocuSoftCRM.Infrastructure.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Identity.ViewModels
{
    public class ProjectFilterDto
    {
        public LoadMoreDto? Filter { get; set; }
        public string? ProjectName { get; set; }
    }
}
