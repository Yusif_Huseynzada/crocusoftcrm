﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Identity.ViewModels
{
    public class AllDailyReportDto
    {
        public IEnumerable<DailyReportDto> Data { get; set; }
        public int TotalCount { get; set; }
    }
}
