﻿using CrocuSoftCRM.Infrastructure.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Identity.ViewModels
{
    public class DailyReportFilterDto
    {
        public LoadMoreDto? Filter { get; set; }
        public List<string>? ProjectName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
       public List<int>? UserIds { get; set; }

        public void NormalizeDates()
        {
            StartDate = StartDate?.Date.ToUniversalTime();
            EndDate = EndDate?.Date.ToUniversalTime();
        }
    }
}
