﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Identity.ViewModels
{
    public class TeamDto
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
    }
}
