﻿using CrocuSoftCRM.Infrastructure.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Identity.ViewModels
{
    public class UserDailyReportFilterDto
    {
        public int UserId { get; set; }
        public LoadMoreDto? Filter { get; set; }
        public DateTime? StartDate { get; set; } 
        public DateTime? EndDate { get; set; }

        public void NormalizeDates()
        {
            StartDate = StartDate?.Date.ToUniversalTime();
            EndDate = EndDate?.Date.ToUniversalTime();    
        }
    }
}
