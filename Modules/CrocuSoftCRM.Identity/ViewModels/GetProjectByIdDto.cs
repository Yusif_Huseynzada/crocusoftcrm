﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Identity.ViewModels
{
    public class GetProjectByIdDto
    {
        public int Id { get; set; }
        public string ProjectName { get; set; }
        public List<GetProjectUsersDto> ProjectUsers { get; set; }
    }
}
