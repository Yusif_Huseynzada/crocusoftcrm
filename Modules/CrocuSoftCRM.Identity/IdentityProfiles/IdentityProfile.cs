﻿using AutoMapper;
using CrocuSoftCRM.Domain.AggregatesModel.DailyReportAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.RoleAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.TeamAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Identity.ViewModels;

namespace CrocuSoftCRM.Identity.IdentityProfiles
{
    public class IdentityProfile : Profile
    {
        public IdentityProfile()
        {
            CreateMap<User, UserProfileDto>();
            CreateMap<User, GetUserByIdDto>()
            .ForMember(dest => dest.Projects, opt => opt.MapFrom(src => src.UserProjects.Select(up => up.Project)));
            CreateMap<User, AdminDto>();
            CreateMap<Role, RoleDto>();
            CreateMap<Team, TeamDto>();
            CreateMap<Team, GetTeamByIdDto>()
                .ForMember(dest => dest.TeamUsers, opt => opt.MapFrom(src => src.Users));
            CreateMap<User, GetTeamUsersDto>();
            CreateMap<Project, ProjectDto>();
            CreateMap<Project, ProjectFilterDto>();
            CreateMap<Project, GetProjectByIdDto>()
              .ForMember(dest => dest.ProjectUsers, opt => opt.MapFrom(src => src.UserProjects.Select(up => up.User)));
            CreateMap<User, GetProjectUsersDto>();
            CreateMap<DailyReport, DailyReportDto>();
            CreateMap<DailyReport, GetDailyReportByIdDto>();
            CreateMap<User, GetDailyReportUserDto>();
            CreateMap<DailyReport, GetUserDailyReportDto>();
            CreateMap<DailyReport, GetUserDailyReportByIdDto>();
               


        }
    }
}
