﻿using AutoMapper;
using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.TeamAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Identity.ViewModels;
using CrocuSoftCRM.Infrastructure.Constants;
using CrocuSoftCRM.Infrastructure.Database;
using CrocuSoftCRM.SharedKernel.Domain.Seedwork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Identity.Queries
{
    public class TeamQueries : ITeamQueries
    {
        private readonly CrocuSoftCRMDbContext _context;
        private readonly IMapper _mapper;

        public TeamQueries(CrocuSoftCRMDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<AllTeamDto> GetAllAsync(LoadMoreDto loadMore)
        {
            var entities = _context.Teams.AsQueryable();
            var count = (await entities.ToListAsync()).Count;

            var teamModel = _mapper.Map<IEnumerable<TeamDto>>(entities);
            AllTeamDto outputModel = new AllTeamDto()
            {
                Data = teamModel,
                TotalCount = count
            };
            return outputModel;
        }

        public async Task<GetTeamByIdDto> GetTeamById(int id)
        {
            var entity = await _context.Teams.Include(p => p.Users).FirstOrDefaultAsync(p => p.Id == id);

            if (entity == null)
                throw new ArgumentNullException();

            return _mapper.Map<GetTeamByIdDto>(entity);
        }

        public async Task<Team> FindAsync(int teamId)
        {
            return await _context.Teams.FirstOrDefaultAsync(p => p.Id == teamId);
        }

        public async Task<Team> FindByNameAsync(string teamName)
        {
            return await _context.Teams.FirstOrDefaultAsync(u => u.TeamName == teamName);
        }

        public async Task<Team> GetTeamEntityAsync(int? teamId)
        {
            var team = await _context.Teams
               .Where(u => u.Id == teamId)
               .AsNoTracking()
               .SingleOrDefaultAsync();

            if (team == null) return null;

            return team;
        }
    }
}
