﻿using CrocuSoftCRM.Domain.AggregatesModel.TeamAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Identity.ViewModels;
using CrocuSoftCRM.Infrastructure.Constants;
using CrocuSoftCRM.SharedKernel.Infrastructure.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Identity.Queries
{
    public interface ITeamQueries : IQuery
    {
        public Task<AllTeamDto> GetAllAsync(LoadMoreDto loadMore);
        public Task<GetTeamByIdDto> GetTeamById(int id);
        Task<Team> GetTeamEntityAsync(int? teamId);
        Task<Team> FindByNameAsync(string teamName);
        Task<Team> FindAsync(int teamId);
    }
}
