﻿using CrocuSoftCRM.Domain.AggregatesModel.DailyReportAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Identity.ViewModels;
using CrocuSoftCRM.Infrastructure.Constants;
using CrocuSoftCRM.SharedKernel.Infrastructure.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Identity.Queries
{
    public interface IDailyReportQueries : IQuery
    {
        public Task<AllDailyReportDto> GetAllAsync(DailyReportFilterDto loadMore);
        public Task<GetDailyReportByIdDto> GetDailyReportById(int id);
        public Task<GetUserDailyReportByIdDto> GetUserDailyReportsByUserId(UserDailyReportFilterDto loadMore);
        Task<DailyReport> GetDailyReportEntityAsync(int? dailyreportId);
        Task<DailyReport> FindAsync(int dailyreportId);
    }
}
