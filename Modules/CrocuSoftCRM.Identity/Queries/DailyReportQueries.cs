﻿using AutoMapper;
using CrocuSoftCRM.Domain.AggregatesModel.DailyReportAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.RoleAggregate;
using CrocuSoftCRM.Identity.ViewModels;
using CrocuSoftCRM.Infrastructure.Constants;
using CrocuSoftCRM.Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CrocuSoftCRM.Identity.Queries
{
    public class DailyReportQueries : IDailyReportQueries
    {
        private readonly CrocuSoftCRMDbContext _context;
        private readonly IMapper _mapper;

        public DailyReportQueries(CrocuSoftCRMDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<AllDailyReportDto> GetAllAsync(DailyReportFilterDto loadMore)
        {
            loadMore.NormalizeDates();
            var entities = _context.DailyReports.Include(p => p.Project).Include(p => p.User).AsQueryable();

            entities = entities.Where(p =>
            (loadMore.ProjectName == null || !loadMore.ProjectName.Any() || loadMore.ProjectName.Contains(p.Project.ProjectName)) &&
            (loadMore.UserIds == null || !loadMore.UserIds.Any() || loadMore.UserIds.Contains(p.UserId)) &&
           (loadMore.StartDate == null || p.CreatedAt.Date >= loadMore.StartDate) &&
            (loadMore.EndDate == null || p.CreatedAt.Date <= loadMore.EndDate));

            var count = (await entities.ToListAsync()).Count;

            if (loadMore.Filter?.SortField != null)
            {
                if (loadMore.Filter.OrderBy)
                {
                    entities = entities.OrderBy($"p=>p.{loadMore.Filter.SortField}");
                }
                else
                {
                    entities = entities.OrderBy($"p=>p.{loadMore.Filter.SortField} descending");
                }
            }
            if (loadMore.Filter?.Skip != null && loadMore.Filter.Take != null)
            {
                entities = entities.Skip(loadMore.Filter.Skip.Value).Take(loadMore.Filter.Take.Value);
            }

            var projectModel = _mapper.Map<IEnumerable<DailyReportDto>>(entities);
            AllDailyReportDto outputModel = new AllDailyReportDto()
            {
                Data = projectModel,
                TotalCount = count
            };
            return outputModel;
        }

        public async Task<GetDailyReportByIdDto> GetDailyReportById(int id)
        {
            var entity = await _context.DailyReports.Include(p => p.Project).Include(p => p.User).FirstOrDefaultAsync(p => p.Id == id);

            if (entity == null)
                throw new ArgumentNullException();

            return _mapper.Map<GetDailyReportByIdDto>(entity);
        }

        public async Task<GetUserDailyReportByIdDto> GetUserDailyReportsByUserId(UserDailyReportFilterDto loadMore)
        {
            loadMore.NormalizeDates();
            var query = _context.DailyReports
                .Include(p => p.Project)
                .Include(p => p.User)
                .Where(p => p.UserId == loadMore.UserId);

            query = query.Where(p =>
            (loadMore.StartDate == null || p.CreatedAt.Date >= loadMore.StartDate) &&
            (loadMore.EndDate == null || p.CreatedAt.Date <= loadMore.EndDate));

            var totalReportCount = await query.CountAsync();

            if (totalReportCount == 0)
                throw new ArgumentNullException();

            if (loadMore.Filter != null)
            {
                if (!string.IsNullOrEmpty(loadMore.Filter.SortField))
                {
                    if (loadMore.Filter.OrderBy)
                    {
                        query = query.OrderBy($"p=>p.{loadMore.Filter.SortField}");
                    }
                    else
                    {
                        query = query.OrderBy($"p=>p.{loadMore.Filter.SortField} descending");
                    }
                }

                if (loadMore.Filter.Skip != null && loadMore.Filter.Take != null)
                {
                    query = query.Skip(loadMore.Filter.Skip.Value).Take(loadMore.Filter.Take.Value);
                }
            }

            var entities = await query.ToListAsync();
            var userDailyReportsDto = _mapper.Map<List<GetUserDailyReportDto>>(entities);

            var userDailyReportByIdDto = new GetUserDailyReportByIdDto
            {
                UserDailyReports = userDailyReportsDto,
                TotalCount = totalReportCount
            };

            return userDailyReportByIdDto;
        }

        public async Task<DailyReport> FindAsync(int dailyReportId)
        {
            return await _context.DailyReports.FirstOrDefaultAsync(p => p.Id == dailyReportId);
        }

        public async Task<DailyReport> GetDailyReportEntityAsync(int? dailyReportId)
        {
            var dailyReport = await _context.DailyReports
               .Where(u => u.Id == dailyReportId)
               .AsNoTracking()
               .SingleOrDefaultAsync();

            if (dailyReport == null) return null;

            return dailyReport;
        }
    }
}
