﻿using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Identity.ViewModels;
using CrocuSoftCRM.Infrastructure.Constants;
using CrocuSoftCRM.SharedKernel.Infrastructure.Queries;
using System.Threading.Tasks;
using CrocuSoftCRM.SharedKernel.Infrastructure.Queries;

namespace CrocuSoftCRM.Identity.Queries
{
    public interface IUserQueries : IQuery
    {
        Task<User> FindByEmailAsync(string email);
        Task<User> FindAsync(int userId);
        Task<UserProfileDto> GetUserProfileAsync(int userId);
        Task<User> GetUserEntityAsync(int? userId);
        public Task<AllUserProfileDto> GetAllUserAsync(UserFilterDto loadMore);
        public Task<AllAdminDto> GetAllAdminAsync(LoadMoreDto loadMore);
        public Task<AdminDto> GetAdminById(int id);
        public Task<GetUserByIdDto> GetUserById(int id);
        Task<IEnumerable<User>> GetUsersByIdsAsync(IEnumerable<int> userIds);

    }
}
