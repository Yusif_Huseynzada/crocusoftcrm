﻿using AutoMapper;
using CrocuSoftCRM.Domain.AggregatesModel.RoleAggregate;
using CrocuSoftCRM.Identity.ViewModels;
using CrocuSoftCRM.Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Identity.Queries
{
    public class RoleQueries : IRoleQueries
    {
        private readonly CrocuSoftCRMDbContext _context;
        private readonly IMapper _mapper;

        public RoleQueries(CrocuSoftCRMDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<RoleDto>> GetAllAsync()
        {
            var entities = await _context.Roles.ToListAsync();

            return _mapper.Map<IEnumerable<RoleDto>>(entities);
        }

        public async Task<Role> GetByName(RoleParametr role)
        {
            var entity = await _context.Roles.FirstOrDefaultAsync(p => p.Name == role.Name);

            return entity;
        }
    }
}
