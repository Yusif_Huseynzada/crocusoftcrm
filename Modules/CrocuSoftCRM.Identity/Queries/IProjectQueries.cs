﻿using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.TeamAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Identity.ViewModels;
using CrocuSoftCRM.Infrastructure.Constants;
using CrocuSoftCRM.SharedKernel.Infrastructure.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Identity.Queries
{
    public interface IProjectQueries : IQuery
    {
        public Task<AllProjectDto> GetAllAsync(ProjectFilterDto loadMore);
        public Task<GetProjectByIdDto> GetProjectById(int id);
        Task<Project> GetProjectEntityAsync(int? projectId);
        Task<Project> FindByNameAsync(string projectName);
        Task<Project> FindAsync(int projectId);
    }
}
