﻿using CrocuSoftCRM.Domain.AggregatesModel.RoleAggregate;
using CrocuSoftCRM.Identity.ViewModels;
using CrocuSoftCRM.SharedKernel.Infrastructure.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Identity.Queries
{
    public interface IRoleQueries : IQuery
    {
        Task<IEnumerable<RoleDto>> GetAllAsync();
        Task<Role> GetByName(RoleParametr role);
    }
}
