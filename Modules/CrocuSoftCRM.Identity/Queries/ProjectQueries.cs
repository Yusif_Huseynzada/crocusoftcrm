﻿using AutoMapper;
using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Identity.ViewModels;
using CrocuSoftCRM.Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace CrocuSoftCRM.Identity.Queries
{
    public class ProjectQueries : IProjectQueries
    {
        private readonly CrocuSoftCRMDbContext _context;
        private readonly IMapper _mapper;

        public ProjectQueries(CrocuSoftCRMDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<AllProjectDto> GetAllAsync(ProjectFilterDto loadMore)
        {
            var entities = _context.Projects.Where(p =>
            loadMore.ProjectName == null || p.ProjectName.ToLower()
            .Contains(loadMore.ProjectName.ToLower()))
            .AsQueryable();
            var count = (await entities.ToListAsync()).Count;

            if (loadMore.Filter?.SortField != null)
            {
                if (loadMore.Filter.OrderBy)
                {
                    entities = entities.OrderBy($"p=>p.{loadMore.Filter.SortField}");
                }
                else
                {
                    entities = entities.OrderBy($"p=>p.{loadMore.Filter.SortField} descending");
                }
            }
            if (loadMore.Filter?.Skip != null && loadMore.Filter.Take != null)
            {
                entities = entities.Skip(loadMore.Filter.Skip.Value).Take(loadMore.Filter.Take.Value);
            }

            var projectModel = _mapper.Map<IEnumerable<ProjectDto>>(entities);
            AllProjectDto outputModel = new AllProjectDto()
            {
                Data = projectModel,
                TotalCount = count
            };
            return outputModel;
        }

        public async Task<GetProjectByIdDto> GetProjectById(int id)
        {
            var entity = await _context.Projects.Include(p => p.UserProjects).ThenInclude(p => p.User).FirstOrDefaultAsync(p => p.Id == id);

            if (entity == null)
                throw new ArgumentNullException();

            return _mapper.Map<GetProjectByIdDto>(entity);
        }

        public async Task<Project> FindAsync(int projectId)
        {
            return await _context.Projects.FirstOrDefaultAsync(p => p.Id == projectId);
        }

        public async Task<Project> FindByNameAsync(string projectName)
        {
            return await _context.Projects.FirstOrDefaultAsync(u => u.ProjectName == projectName);
        }

        public async Task<Project> GetProjectEntityAsync(int? projectId)
        {
            var project = await _context.Projects
               .Where(u => u.Id == projectId)
               .AsNoTracking()
               .SingleOrDefaultAsync();

            if (project == null) return null;

            return project;
        }
        
    }
}
