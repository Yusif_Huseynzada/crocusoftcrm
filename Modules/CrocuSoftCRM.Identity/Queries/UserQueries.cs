﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Identity.ViewModels;
using CrocuSoftCRM.Infrastructure.Constants;
using CrocuSoftCRM.Infrastructure.Database;
using CrocuSoftCRM.SharedKernel.Domain.Seedwork;
using System.Linq.Dynamic.Core;
using MediatR;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace CrocuSoftCRM.Identity.Queries
{
    public class UserQueries : IUserQueries
    {
        private readonly CrocuSoftCRMDbContext _context;
        private readonly IMapper _mapper;

        public UserQueries(CrocuSoftCRMDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<User> FindAsync(int userId)
        {
            return await _context.Users.Include(p => p.Role).FirstOrDefaultAsync(p => p.Id == userId);
        }

        public async Task<User> FindByEmailAsync(string email)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.Email == email);
        }

        public async Task<UserProfileDto> GetUserProfileAsync(int userId)
        {
            var user = await _context.Users
                .Where(u => u.Id == userId)
                .AsNoTracking()
                .SingleOrDefaultAsync();

            if (user == null) return null;
            var profile = _mapper.Map<UserProfileDto>(user);
            return profile;
        }

        public async Task<User> GetUserEntityAsync(int? userId)
        {
            var user = await _context.Users
               .Where(u => u.Id == userId)
               .AsNoTracking()
               .SingleOrDefaultAsync();

            if (user == null) return null;

            return user;
        }

        public async Task<AllUserProfileDto> GetAllUserAsync(UserFilterDto loadMore)
        {
            var entities = _context.Users.Where(p =>
           (loadMore.FirstName == null || p.FirstName.ToLower().Contains(loadMore.FirstName.ToLower())) &&
           (loadMore.LastName == null || p.LastName.ToLower().Contains(loadMore.LastName.ToLower())))
        .Include(p => p.Role).Include(p => p.Role).Include(p => p.Team).Include(p => p.UserProjects).ThenInclude(p => p.Project).AsQueryable();

            if (loadMore.TeamNames != null && loadMore.TeamNames.Any())
            {
                entities = entities.Where(p => p.Team != null && loadMore.TeamNames.Contains(p.Team.TeamName));
            }

            if (loadMore.ProjectNames != null && loadMore.ProjectNames.Any())
            {
                entities = entities.Where(p => p.UserProjects.Any(up => loadMore.ProjectNames.Contains(up.Project.ProjectName)));
            }

            var count = (await entities.ToListAsync()).Count;
            if (loadMore.Filter?.SortField != null)
            {
                if (loadMore.Filter.OrderBy)
                {
                    entities = entities.OrderBy($"p=>p.{loadMore.Filter.SortField}");
                }
                else
                {
                    entities = entities.OrderBy($"p=>p.{loadMore.Filter.SortField} descending");
                }
            }
            if (loadMore.Filter?.Skip != null && loadMore.Filter.Take != null)
            {
                entities = entities.Skip(loadMore.Filter.Skip.Value).Take(loadMore.Filter.Take.Value);
            }

            var userModel = _mapper.Map<IEnumerable<UserProfileDto>>(entities);
            AllUserProfileDto outputModel = new AllUserProfileDto()
            {
                Data = userModel,
                TotalCount = count
            };

            return outputModel;
        }

        public async Task<AllAdminDto> GetAllAdminAsync(LoadMoreDto loadMore)
        {
            var entities = _context.Users.Include(p => p.Role).Where(p => p.Role.Name == CustomRole.SuperAdmin || p.Role.Name == CustomRole.Admin).AsQueryable();
            var count = (await entities.ToListAsync()).Count;

            if (loadMore.SortField != null)
            {
                if (loadMore.OrderBy)
                {
                    entities = entities.OrderBy($"p=>p.{loadMore.SortField}");
                }
                else
                {
                    entities = entities.OrderBy($"p=>p.{loadMore.SortField} descending");
                }
            }
            if (loadMore.Skip != null && loadMore.Take != null)
            {
                entities = entities.Skip(loadMore.Skip.Value).Take(loadMore.Take.Value);
            }

            var userModel = _mapper.Map<IEnumerable<AdminDto>>(entities);
            AllAdminDto outputModel = new AllAdminDto()
            {
                Data = userModel,
                TotalCount = count
            };

            return outputModel;
        }

        public async Task<AdminDto> GetAdminById(int id)
        {
            var entity = await _context.Users.Include(p => p.Role).FirstOrDefaultAsync(p => p.Id == id && p.Role.Name == CustomRole.SuperAdmin || p.Role.Name == CustomRole.Admin);

            if (entity == null)
                throw new ArgumentNullException();

            return _mapper.Map<AdminDto>(entity);
        }

        public async Task<GetUserByIdDto> GetUserById(int id)
        {
            var entity = await _context.Users.Include(p => p.Role).Include(p => p.Team).Include(p => p.UserProjects).ThenInclude(p => p.Project).FirstOrDefaultAsync(p => p.Id == id);

            if (entity == null)
                throw new ArgumentNullException();

            return _mapper.Map<GetUserByIdDto>(entity);
        }

        public async Task<IEnumerable<User>> GetUsersByIdsAsync(IEnumerable<int> userIds)
        {
            return await _context.Users.Where(u => userIds.Contains(u.Id)).ToListAsync();
        }

    }
}
