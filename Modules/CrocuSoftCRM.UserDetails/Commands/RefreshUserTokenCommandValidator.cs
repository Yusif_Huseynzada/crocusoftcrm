﻿using FluentValidation;

namespace CrocuSoftCRM.UserDetails.Commands
{
    public class RefreshUserTokenCommandValidator : AbstractValidator<RefreshUserTokenCommand>
    {
        public RefreshUserTokenCommandValidator() : base()
        {
            RuleFor(command => command.RefreshToken).NotNull();
        }
    }
}
