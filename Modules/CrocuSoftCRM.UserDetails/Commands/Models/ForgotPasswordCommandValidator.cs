﻿using FluentValidation;

namespace CrocuSoftCRM.UserDetails.Commands.Models
{
    class ForgotPasswordCommandValidator : AbstractValidator<ForgotPasswordCommand>
    {
        public ForgotPasswordCommandValidator() : base()
        {
            RuleFor(command => command.Email).NotNull();
        }
    }
}
