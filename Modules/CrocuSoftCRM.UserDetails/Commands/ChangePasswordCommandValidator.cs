﻿using FluentValidation;

namespace CrocuSoftCRM.UserDetails.Commands
{
    public class ChangePasswordCommandValidator : AbstractValidator<ChangePasswordCommand>
    {
        public ChangePasswordCommandValidator() : base()
        {
            RuleFor(command => command.OldPassword).NotNull();
            RuleFor(command => command.NewPassword).NotNull();
            RuleFor(command => command.NewPasswordConfirm).NotNull();
        }
    }
}
