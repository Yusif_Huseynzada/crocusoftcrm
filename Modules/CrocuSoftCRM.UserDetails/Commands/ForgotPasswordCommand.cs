﻿using MediatR;

namespace CrocuSoftCRM.UserDetails.Commands
{
    public class ForgotPasswordCommand : IRequest<bool>
    {
        public string Email { get; set; }
    }
}
