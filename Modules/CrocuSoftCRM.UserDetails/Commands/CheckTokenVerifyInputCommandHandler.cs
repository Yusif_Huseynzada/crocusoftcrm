﻿using CrocuSoftCRM.Identity.Auth;
using CrocuSoftCRM.Identity.Queries;
using CrocuSoftCRM.Infrastructure;
using CrocuSoftCRM.Infrastructure.Commands;
using CrocuSoftCRM.Infrastructure.Idempotency;
using CrocuSoftCRM.UserDetails.Commands.Models;
using MediatR;
using Microsoft.Extensions.Options;
using System;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;

namespace CrocuSoftCRM.UserDetails.Commands
{
    public class CheckTokenVerifyInputCommandHandler : IRequestHandler<CheckTokenVerifyInputCommand, bool>
    {
        private readonly IUserManager _userManager;
        private readonly IUserQueries _userQueries;
        private readonly CrocuSoftCRMSettings _crocuSoftCRMSettings;


        public CheckTokenVerifyInputCommandHandler(IUserManager userManager, IUserQueries userQueries, IOptions<CrocuSoftCRMSettings> saffarmaSettings)
        {
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _userQueries = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
            _crocuSoftCRMSettings = saffarmaSettings.Value ?? throw new ArgumentNullException(nameof(saffarmaSettings));
        }

        public async Task<bool> Handle(CheckTokenVerifyInputCommand request, CancellationToken cancellationToken)
        {
            var email = request.Email.ToLower();
            var user = await _userQueries.FindByEmailAsync(email);
            var userId = user.Id.ToString();  //because of validateTokenUserId , it accepts only String value

            if (user == null)
                throw new AuthenticationException("Invalid credentials.");

            var validateTokenUserId = TokenManager.ValidateToken(_crocuSoftCRMSettings, request.Token);

            if (validateTokenUserId == null)
            {
                throw new AuthenticationException("Token is null");
            }


            if (!userId.Equals(validateTokenUserId))
            {
                throw new AuthenticationException("Token is invalid for this user");
            }

            return true;
        }

        public class CheckTokenInputCommandHandler : IdentifiedCommandHandler<CheckTokenVerifyInputCommand, bool>
        {
            public CheckTokenInputCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
            {
            }

            protected override bool CreateResultForDuplicateRequest()
            {
                return true;
            }
        }
    }
}
