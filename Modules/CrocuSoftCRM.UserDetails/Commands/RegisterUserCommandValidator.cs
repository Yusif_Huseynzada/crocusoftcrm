﻿using FluentValidation;

namespace CrocuSoftCRM.UserDetails.Commands
{
    public class RegisterUserCommandValidator : AbstractValidator<RegisterUserCommand>
    {
        public RegisterUserCommandValidator() : base()
        {
            RuleFor(command => command.Password).NotNull().MinimumLength(8);
            RuleFor(command => command.Email).NotNull();
            RuleFor(command => command.FirstName).NotNull();
            RuleFor(command => command.Phone).NotNull();
            RuleFor(command => command.RoleId).NotNull().GreaterThan(0);
            RuleFor(command => command.TeamId).GreaterThanOrEqualTo(0); 
        }
        
    }
}
