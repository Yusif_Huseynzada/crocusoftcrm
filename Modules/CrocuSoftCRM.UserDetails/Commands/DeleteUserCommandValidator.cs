﻿using FluentValidation;

namespace CrocuSoftCRM.UserDetails.Commands
{
    public class DeleteUserCommandValidator : AbstractValidator<DeleteUserCommand>
    {
        public DeleteUserCommandValidator() : base()
        {
            RuleFor(command => command.Id).NotNull();
            RuleFor(command => command.IsDeleted).NotNull().WithMessage("IsDeleted field is required.");
        }
    }
}
