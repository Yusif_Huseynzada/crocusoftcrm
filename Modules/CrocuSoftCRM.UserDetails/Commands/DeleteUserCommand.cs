﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace CrocuSoftCRM.UserDetails.Commands
{
    public class DeleteUserCommand : IRequest<bool>
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}
