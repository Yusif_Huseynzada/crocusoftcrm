﻿using MediatR;

namespace CrocuSoftCRM.UserDetails.Commands
{
    public class UpdateUserCommand : IRequest<bool>
    {
        public int? Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsActive { get; set; }
        public int? RoleId { get; set; }
        public int? TeamId { get; set; }

    }
}
