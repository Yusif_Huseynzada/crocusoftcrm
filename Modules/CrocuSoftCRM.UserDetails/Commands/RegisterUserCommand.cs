﻿using MediatR;

namespace CrocuSoftCRM.UserDetails.Commands
{
    public class RegisterUserCommand : IRequest<bool>
    {
        public int LoggedInUserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public int RoleId { get; set; }
        public int? TeamId { get; set; }
    }
}
