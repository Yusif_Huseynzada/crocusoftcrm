﻿using FluentValidation;

namespace CrocuSoftCRM.UserDetails.Commands
{
    public class UpdateUserCommandValidator : AbstractValidator<UpdateUserCommand>
    {
        public UpdateUserCommandValidator() : base()
        {
            RuleFor(command => command.Id).NotNull();
            RuleFor(command => command.Email).NotNull();
            RuleFor(command => command.FirstName).NotNull();
            RuleFor(command => command.LastName).NotNull();
            RuleFor(command => command.Phone).NotNull();
            RuleFor(command => command.RoleId).GreaterThan(0);
            RuleFor(command => command.TeamId).GreaterThanOrEqualTo(0);
        }
    }
}
