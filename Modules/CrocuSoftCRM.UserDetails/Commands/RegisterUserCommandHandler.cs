﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.RoleAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.TeamAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Domain.Exceptions;
using CrocuSoftCRM.Identity.Queries;
using CrocuSoftCRM.Infrastructure.Commands;
using CrocuSoftCRM.Infrastructure.Database;
using CrocuSoftCRM.Infrastructure.Idempotency;
using CrocuSoftCRM.SharedKernel.Infrastructure;
using CrocuSoftCRM.SharedKernel.Domain.Seedwork;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace CrocuSoftCRM.UserDetails.Commands
{
    public class RegisterUserCommandHandler : IRequestHandler<RegisterUserCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserQueries _userQueries;
        private readonly CrocuSoftCRMDbContext _context;

        public RegisterUserCommandHandler(IUserRepository userRepository, IUserQueries userQueries, CrocuSoftCRMDbContext context)
        {
            _userRepository = userRepository;
            _userQueries = userQueries;
            _context = context;
        }

        public async Task<bool> Handle(RegisterUserCommand request, CancellationToken cancellationToken)
        {
            var loggedInUserId = request.LoggedInUserId;
            var loggedInUser = await _context.Users.FindAsync(loggedInUserId);
            var email = request.Email;
            if (!BeValidEmail(request.Email))
            {
                throw new DomainException($"Invalid email format. Email must end with @crocusoft.com.");
            }

            var existingEmailUser = await _userQueries.FindByEmailAsync(request.Email);
            if (existingEmailUser != null)
            {
                throw new DomainException($"User with email '{request.Email}' already exists.");
            }

            var user = new User(request.Email.Trim(), request.Phone.Trim(), PasswordHasher.HashPassword(request.Password), request.FirstName, request.LastName);

            var role = await _context.Roles.FirstOrDefaultAsync(p => p.Id == request.RoleId);
            if (role != null)
            {
                // Kullanıcının rolünü kontrol edin
                if (!loggedInUser.CanCreateUser() && role.Name == RoleName.Admin)
                {
                    throw new DomainException($"You don't have permission to create an admin user.");
                }

                user.SetRole(role.Id);
            }

            if (request.TeamId.HasValue)
            {
                var team = await _context.Teams.FirstOrDefaultAsync(t => t.Id == request.TeamId.Value);
                if (team != null)
                {
                    user.SetTeam(team.Id);
                }
            }

            await _userRepository.AddAsync(user);
            await _userRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);

            return true;
        }
        private bool BeValidEmail(string email)
        {
            return email.EndsWith("@crocusoft.com", StringComparison.OrdinalIgnoreCase);
        }
    }

    public class RegisterUserIdentifiedCommandHandler :
        IdentifiedCommandHandler<RegisterUserCommand, bool>
    {
        public RegisterUserIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true; // Ignore duplicate requests
        }
    }
}
