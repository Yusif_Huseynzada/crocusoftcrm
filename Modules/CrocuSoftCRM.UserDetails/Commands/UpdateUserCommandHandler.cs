﻿using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Domain.Exceptions;
using CrocuSoftCRM.Identity.Queries;
using CrocuSoftCRM.Infrastructure.Commands;
using CrocuSoftCRM.Infrastructure.Database;
using CrocuSoftCRM.Infrastructure.Idempotency;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CrocuSoftCRM.UserDetails.Commands
{
    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserQueries _userQueries;
        private readonly CrocuSoftCRMDbContext _context;

        public UpdateUserCommandHandler(IUserRepository userRepository, IUserQueries userQueries, CrocuSoftCRMDbContext context)
        {
            _userRepository = userRepository;
            _userQueries = userQueries;
            _context = context;
        }

        public async Task<bool> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _userQueries.GetUserEntityAsync(request.Id);


            if (!BeValidEmail(request.Email))
            {
                throw new DomainException($"Invalid email format. Email must end with @crocusoft.com.");
            }

            user.SetDetails(request.Email.Trim(), request.Phone.Trim(), request.FirstName, request.LastName);

            if (request.RoleId.HasValue)
            {
                var role = await _context.Roles.FirstOrDefaultAsync(p => p.Id == request.RoleId);
                if (role != null)
                {
                    user.SetRole(role.Id);
                }
            }

            if (request.TeamId.HasValue)
            {
                var team = await _context.Teams.FirstOrDefaultAsync(t => t.Id == request.TeamId.Value);
                if (team != null)
                {
                    user.SetTeam(team.Id);
                }
            }


            _userRepository.UpdateAsync(user);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return true;
        }
        private bool BeValidEmail(string email)
        {
            return email.EndsWith("@crocusoft.com", StringComparison.OrdinalIgnoreCase);
        }
    }

    public class UpdateUserIdentifiedCommandHandler : IdentifiedCommandHandler<UpdateUserCommand, bool>
    {
        public UpdateUserIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true; // Ignore duplicate requests
        }
    }
}
