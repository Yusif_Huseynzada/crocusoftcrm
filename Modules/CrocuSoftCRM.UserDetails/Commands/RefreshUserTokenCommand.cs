﻿using CrocuSoftCRM.UserDetails.Commands.Models;
using MediatR;

namespace CrocuSoftCRM.UserDetails.Commands
{
    public class RefreshUserTokenCommand : IRequest<JwtTokenDTO>
    {
        public string RefreshToken { get; set; }
    }
}
