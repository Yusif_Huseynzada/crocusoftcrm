﻿using MediatR;

namespace CrocuSoftCRM.UserDetails.Commands
{
    public class CheckTokenVerifyInputCommand : IRequest<bool>
    {
        public string Token { get; set; }

        public string Email { get; set; }
    }
}
