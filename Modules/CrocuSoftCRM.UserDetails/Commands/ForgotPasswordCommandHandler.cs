﻿using MediatR;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Domain.Exceptions;
using CrocuSoftCRM.Identity.Queries;
using CrocuSoftCRM.Infrastructure.Commands;
using CrocuSoftCRM.Infrastructure.Idempotency;
using CrocuSoftCRM.SharedKernel.Infrastructure;
using CrocuSoftCRM.SharedKernel.Infrastructure.Helper;

namespace CrocuSoftCRM.UserDetails.Commands
{
    public class ForgotPasswordCommandHandler : IRequestHandler<ForgotPasswordCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserQueries _userService;
        private readonly ISendEmailAsync _sendEmailAsync;

        public ForgotPasswordCommandHandler(IUserRepository userRepository, IUserQueries userQueries, ISendEmailAsync sendEmailAsync)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _userService = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
            _sendEmailAsync = sendEmailAsync;
        }

        public async Task<bool> Handle(ForgotPasswordCommand request, CancellationToken cancellationToken)
        {
            var user = await _userService.FindByEmailAsync(request.Email);

            if (user == null)
            {
                throw new DomainException("Provided user name is not associated with any account");
            }

            if (string.IsNullOrEmpty(user.Email))
            {
                throw new DomainException("User does not have an email address");
            }

            var newPassword = Guid.NewGuid().ToString("N").Substring(0, 8);

            var newPasswordHash = PasswordHasher.HashPassword(newPassword);
            user.ResetPassword(newPasswordHash);

            _userRepository.UpdateAsync(user);
            await _userRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);

            var subject = "Şifrəniz yeniləndi";
            string body = @"<html><body><p><strong>Hörmətli, " + user.FirstName + " " + user.LastName + ", </strong> </p><br></br><p>test tətbiqi üçün istifadəçi şifrəniz yeniləndi. Yeni şifrəniz aşağıdakı kimidir:<br></br> <br></br><strong>" + newPassword + "<strong></p><br></br> Hər hansı çətinliyiniz olarsa sistem administratoru ilə əlaqə saxlamağınız tövsiyyə olunur.</p><br><p> Hörmətlə,</p><p> Dəyərlisən </p> ";
            _sendEmailAsync.SendEmailUserAsync(user.Email, subject, body,null);

            return true;
        }
    }

    public class ForgotPasswordIdentifiedCommandHandler : IdentifiedCommandHandler<ForgotPasswordCommand, bool>
    {
        public ForgotPasswordIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}
