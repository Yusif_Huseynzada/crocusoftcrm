﻿using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Identity.Auth;
using CrocuSoftCRM.Identity.Queries;
using CrocuSoftCRM.Infrastructure.Commands;
using CrocuSoftCRM.Infrastructure.Idempotency;
using CrocuSoftCRM.NotFound.Exceptions;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CrocuSoftCRM.UserDetails.Commands
{
    public class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserQueries _userQueries;

        public DeleteUserCommandHandler(IUserQueries userQueries, IUserRepository userRepository)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _userQueries = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
        }

        public async Task<bool> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            var project = await _userQueries.FindAsync(request.Id);

            if (project == null)
            {

                throw new NotFoundException($"User with id {request.Id} not found.");
            }

            project.SetIsDeleted(request.IsDeleted);
            _userRepository.UpdateAsync(project);

            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return true;
        }
    }

    public class DeleteUserIdentifiedCommandHandler : IdentifiedCommandHandler<DeleteUserCommand, bool>
    {
        public DeleteUserIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}