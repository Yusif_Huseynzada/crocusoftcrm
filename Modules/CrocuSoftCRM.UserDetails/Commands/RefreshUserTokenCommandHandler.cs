﻿using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Identity.Auth;
using CrocuSoftCRM.Identity.Queries;
using CrocuSoftCRM.Infrastructure.Database;
using CrocuSoftCRM.UserDetails.Commands.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;

namespace CrocuSoftCRM.UserDetails.Commands
{
    public class RefreshUserTokenCommandHandler : IRequestHandler<RefreshUserTokenCommand, JwtTokenDTO>
    {
        private readonly IUserManager _userManager;
        private readonly IUserQueries _userQueries;
        private readonly IUserRepository _userRepository;
        private readonly CrocuSoftCRMDbContext _context;

        public RefreshUserTokenCommandHandler(IUserManager userManager, IUserQueries userQueries,
            IUserRepository userRepository, CrocuSoftCRMDbContext context)
        {
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _userQueries = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public async Task<JwtTokenDTO> Handle(RefreshUserTokenCommand request, CancellationToken cancellationToken)
        {
            var splitToken = request.RefreshToken.Split("_");

            var user = await _context.Users.Include(p => p.Role).FirstOrDefaultAsync(p => p.RefreshToken == request.RefreshToken);
            await _userRepository.UnitOfWork.SaveChangesAsync();

            if (user == null)
                throw new AuthenticationException("Invalid token.");

            if (Convert.ToDateTime(splitToken[2]) < DateTime.Now)
                throw new AuthenticationException("Token is expired.");

            (string token, DateTime expiresAt) = _userManager.GenerateJwtToken(user);

            return new JwtTokenDTO
            {
                Token = token,
                RefreshToken = request.RefreshToken,
                ExpiresAt = expiresAt
            };
        }

    }
}
