﻿using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using System.Collections.Generic;
using System.Security.Claims;

namespace CrocuSoftCRM.Infrastructure.Identity
{
    public interface IClaimsManager
    {
        int GetCurrentUserId();

        string GetCurrentUserName();

        IEnumerable<Claim> GetUserClaims(User user);

        Claim GetUserClaim(string claimType);
    }
}
