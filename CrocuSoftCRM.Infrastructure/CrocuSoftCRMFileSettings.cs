﻿namespace CrocuSoftCRM.Infrastructure
{
    public class CrocuSoftCRMFileSettings
    {
        public static readonly string BasePathWindows = nameof(BasePathWindows);
        public static readonly string BasePathLinux = nameof(BasePathLinux);

        public string Asset { get; set; }
        public string Category { get; set; }
        public string Partner { get; set; }
        public string Campaign { get; set; }
        public string Employee { get; set; }
        public string Notification { get; set; }
    }
}
