﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CrocuSoftCRM.Infrastructure.Idempotency;

namespace CrocuSoftCRM.Infrastructure.EntityConfigurations.AuditEntityConfiguration
{
    internal class ClientRequestEntityTypeConfiguration
    : IEntityTypeConfiguration<ClientRequest>
    {
        public void Configure(EntityTypeBuilder<ClientRequest> requestConfiguration)
        {

        }
    }
}
