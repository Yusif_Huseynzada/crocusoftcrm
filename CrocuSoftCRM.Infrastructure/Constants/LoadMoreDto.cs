﻿namespace CrocuSoftCRM.Infrastructure.Constants
{
    public class LoadMoreDto
    {
        public int? Skip { get; set; }
        public int? Take { get; set; }
        public string SortField { get; set; }
        public bool OrderBy { get; set; }
    }
}
