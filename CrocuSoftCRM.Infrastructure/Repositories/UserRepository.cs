﻿using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Infrastructure.Database;
using CrocuSoftCRM.SharedKernel.Infrastructure;
using Microsoft.EntityFrameworkCore;
using CrocuSoftCRM.SharedKernel.Infrastructure;
using CrocuSoftCRM.Infrastructure.Database;
using CrocuSoftCRM.Domain.AggregatesModel.DailyReportAggregate;

namespace CrocuSoftCRM.Infrastructure.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public sealed override DbContext Context { get; protected set; }

        public UserRepository(CrocuSoftCRMDbContext context)
        {
            Context = context;
        }
    }
}
