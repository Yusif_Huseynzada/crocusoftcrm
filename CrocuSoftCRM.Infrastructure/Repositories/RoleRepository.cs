﻿using CrocuSoftCRM.Domain.AggregatesModel.RoleAggregate;
using CrocuSoftCRM.Infrastructure.Database;
using CrocuSoftCRM.SharedKernel.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace CrocuSoftCRM.Infrastructure.Repositories
{
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public sealed override DbContext Context { get; protected set; }

        public RoleRepository(CrocuSoftCRMDbContext context)
        {
            Context = context;
        }
    }
}
