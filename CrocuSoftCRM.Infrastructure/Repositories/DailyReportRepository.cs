﻿using CrocuSoftCRM.Domain.AggregatesModel.DailyReportAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.RoleAggregate;
using CrocuSoftCRM.Infrastructure.Database;
using CrocuSoftCRM.SharedKernel.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Infrastructure.Repositories
{
    public class DailyReportRepository : Repository<DailyReport>, IDailyReportRepository
    {
        public sealed override DbContext Context { get; protected set; }

        public DailyReportRepository(CrocuSoftCRMDbContext context)
        {
            Context = context;
        }
        public void Delete(DailyReport dailyReport)
        {
            if (dailyReport != null)
            {
                Context.Entry(dailyReport).State = EntityState.Deleted;
                Context.SaveChanges();
            }
        }

    }
}
