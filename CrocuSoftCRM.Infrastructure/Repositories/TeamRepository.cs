﻿using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.TeamAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Infrastructure.Database;
using CrocuSoftCRM.SharedKernel.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Infrastructure.Repositories
{
    public class TeamRepository : Repository<Team>, ITeamRepository
    {
        public sealed override DbContext Context { get; protected set; }

        public TeamRepository(CrocuSoftCRMDbContext context)
        {
            Context = context;
        }
        public void Delete(Team team)
        {
            if (team != null)
            {
                Context.Entry(team).State = EntityState.Deleted;
                Context.SaveChanges();
            }
        }
    }
}
