﻿using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.Infrastructure.Database;
using CrocuSoftCRM.SharedKernel.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Infrastructure.Repositories
{
    public class ProjectRepository : Repository<Project>, IProjectRepository
    {
        public sealed override DbContext Context { get; protected set; }

        public ProjectRepository(CrocuSoftCRMDbContext context)
        {
            Context = context;
        }
        public IQueryable<Project> GetAll()
        {
            return Context.Set<Project>();
        }

        public async Task<Project> FirstOrDefaultAsync(Expression<Func<Project, bool>> predicate)
        {
            return await Context.Set<Project>().FirstOrDefaultAsync(predicate);
        }
        public IQueryable<Project> GetAllIncludingUserProjects()
        {
            return Context.Set<Project>().Include(p => p.UserProjects);
        }
    }
}
