﻿using CrocuSoftCRM.Domain.AggregatesModel.DailyReportAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.RoleAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.TeamAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using MediatR;
using Microsoft.EntityFrameworkCore;
using CrocuSoftCRM.Infrastructure.EntityConfigurations.IdentityEntityConfiguration;
using CrocuSoftCRM.Infrastructure.EntityConfigurations.RoleEntityConfiguration;
using CrocuSoftCRM.SharedKernel.Domain.Seedwork;
using CrocuSoftCRM.Infrastructure.EntityConfigurations.AuditEntityConfiguration;
using CrocuSoftCRM.SharedKernel.Infrastructure;

namespace CrocuSoftCRM.Infrastructure.Database
{
    public class CrocuSoftCRMDbContext: DbContext, IUnitOfWork
    {
        public const string IDENTITY_SCHEMA = "Identity";
        public const string DEFAULT_SCHEMA = "dbo";

        private readonly IMediator _mediator;

        public CrocuSoftCRMDbContext(DbContextOptions options, IMediator mediator) : base(options)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        public DbSet<User> Users { get; private set; }
        public DbSet<Role> Roles { get; private set; }
        public DbSet<DailyReport> DailyReports { get; private set; }
        public DbSet<Project> Projects { get; private set; }
        public DbSet<Team> Teams { get; private set; }
        public DbSet<UserProject> UserProjects { get; private set; }

       

        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            await _mediator.DispatchDomainEventsAsync(this);

            await SaveChangesAsync(true, cancellationToken);

            return true;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ClientRequestEntityTypeConfiguration());
            // Identity
            modelBuilder.ApplyConfiguration(new UserEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new RoleEntityTypeConfiguration());



            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.ClientSetNull;
            }
        }
    }
}
