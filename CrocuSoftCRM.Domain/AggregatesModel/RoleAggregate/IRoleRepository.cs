﻿using CrocuSoftCRM.Domain.AggregatesModel.RoleAggregate;
using CrocuSoftCRM.SharedKernel.Domain.Seedwork;

namespace CrocuSoftCRM.Domain.AggregatesModel.RoleAggregate
{
    public interface IRoleRepository : IRepository<Role>
    {
    }
}
