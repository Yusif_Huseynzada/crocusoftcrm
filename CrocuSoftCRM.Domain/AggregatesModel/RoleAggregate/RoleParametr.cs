﻿using CrocuSoftCRM.SharedKernel.Domain.Seedwork;

namespace CrocuSoftCRM.Domain.AggregatesModel.RoleAggregate
{
    public class RoleParametr : Enumeration
    {
        public static RoleParametr SuperAdmin = new RoleParametr(0, RoleName.SuperAdmin);
        public static RoleParametr Admin = new RoleParametr(1, RoleName.Admin);
        public static RoleParametr Employee = new RoleParametr(2, RoleName.Employee);
        public static RoleParametr Head = new RoleParametr(3, RoleName.Head);

        public RoleParametr(int id, string name) : base(id, name)
        {

        }

        public RoleParametr()
        {

        }
    }
}
