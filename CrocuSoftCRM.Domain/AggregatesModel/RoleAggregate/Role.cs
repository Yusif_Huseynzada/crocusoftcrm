﻿using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.SharedKernel.Domain.Seedwork;

namespace CrocuSoftCRM.Domain.AggregatesModel.RoleAggregate
{
    public class Role : Entity, IAggregateRoot
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public bool IsDeleted { get; private set; }

        private readonly List<User> _users;
        public IReadOnlyCollection<User> Users => _users;

        public Role()
        {
            _users = new List<User>();

        }

        public void SetDetails(string name, string description)
        {
            Name = name;
            Description = description;
        }
    }
}
