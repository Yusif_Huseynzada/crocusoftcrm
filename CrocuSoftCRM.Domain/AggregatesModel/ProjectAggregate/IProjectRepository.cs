﻿using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.SharedKernel.Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate
{
    public interface IProjectRepository : IRepository<Project>
    {
        IQueryable<Project> GetAll();
        Task<Project> FirstOrDefaultAsync(Expression<Func<Project, bool>> predicate);
        IQueryable<Project> GetAllIncludingUserProjects();
    }
}

