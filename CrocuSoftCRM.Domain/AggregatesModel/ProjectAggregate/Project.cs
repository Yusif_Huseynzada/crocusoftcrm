﻿using CrocuSoftCRM.Domain.AggregatesModel.DailyReportAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.RoleAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.TeamAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.SharedKernel.Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate
{
    public class Project : Entity, IAggregateRoot
    {
        public string ProjectName { get; private set; }

        private readonly List<UserProject> _userProjects;
        public IReadOnlyCollection<UserProject> UserProjects => _userProjects;

        private readonly List<DailyReport> _dailyReports;
        public IReadOnlyCollection<DailyReport> DailyReports => _dailyReports;

        public Project()
        {
            _userProjects = new List<UserProject>();
            _dailyReports = new List<DailyReport>();
        }

        public void SetDetails(string projectName)
        {
            ProjectName = projectName;
        }
        public void AddUser(User user)
        {
            var userProject = new UserProject(user, this);
            _userProjects.Add(userProject);
        }

        public void AddUser(UserProject userProject)
        {
            _userProjects.Add(userProject);
        }
        public void RemoveUserCompletely(int userId)
        {
            var userProject = _userProjects.Find(up => up.UserId == userId);
            if (userProject != null)
            {
                // Projeden ve veritabanından kullanıcıyı kaldır
                RemoveUserProject(userProject);
            }
        }

        public void RemoveUserProject(UserProject userProject)
        {
            _userProjects.Remove(userProject);
        }

    }
}
