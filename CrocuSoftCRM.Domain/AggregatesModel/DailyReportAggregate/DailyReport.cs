﻿using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.SharedKernel.Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Domain.AggregatesModel.DailyReportAggregate
{
    public class DailyReport : Entity, IAggregateRoot
    {
        public string Description { get; private set; }
        public DateTime CreatedAt { get; private set; } = DateTime.UtcNow.AddHours(4);
        public int ProjectId { get; private set; }
        public Project Project { get; private set; }
        public int UserId { get; private set; }
        public User User { get; private set; }

        public void SetDetails(string description)
        {
            Description = description;
        }
        public void SetProject(int projectId)
        {
            ProjectId = projectId;
        }

        public void SetUser(int userId)
        {
            UserId = userId;
        }
        public void UpdateCreatedAt(DateTime newCreatedAt)
        {
            CreatedAt = newCreatedAt;
        }
    }
}
