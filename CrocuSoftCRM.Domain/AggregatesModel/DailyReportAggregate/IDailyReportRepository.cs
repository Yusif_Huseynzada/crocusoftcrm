﻿using CrocuSoftCRM.SharedKernel.Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Domain.AggregatesModel.DailyReportAggregate
{
    public interface IDailyReportRepository : IRepository<DailyReport>
    {
        void Delete(DailyReport dailyReport);
    }
}
