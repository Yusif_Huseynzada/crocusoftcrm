﻿using CrocuSoftCRM.Domain.AggregatesModel.DailyReportAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.RoleAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.TeamAggregate;
using CrocuSoftCRM.Domain.Exceptions;
using CrocuSoftCRM.SharedKernel.Domain.Seedwork;

namespace CrocuSoftCRM.Domain.AggregatesModel.UserAggregate
{
    public class User : Entity, IAggregateRoot
    {

        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }
        public string Phone { get; private set; }
        public string PasswordHash { get; private set; }
        public DateTime CreatedAt { get; private set; } = DateTime.UtcNow;
        public bool IsDeleted { get; private set; }
        public bool IsActive { get; private set; }
        public string? RefreshToken { get; private set; }
        public int? TeamId { get; private set; }
        public Team Team { get; private set; }
        public int RoleId { get; private set; }
        public Role Role { get; private set; }

        private readonly List<DailyReport> _dailyReports;
        public IReadOnlyCollection<DailyReport> DailyReports => _dailyReports;

        private readonly List<UserProject> _userProjects;
        public IReadOnlyCollection<UserProject> UserProjects => _userProjects;

        public User()
        {
            _userProjects = new List<UserProject>();
            _dailyReports = new List<DailyReport>();
        }


        public User(string email, string phone, string passwordHash, string firstName, string lastName) : this()
        {
            Email = email;
            Phone = phone;
            PasswordHash = passwordHash;
            FirstName = firstName;
            LastName = lastName;
            IsActive = true;
        }

        public void SetPasswordHash(string oldPasswordHash, string newPasswordHash)
        {
            if (PasswordHash != oldPasswordHash)
            {
                throw new DomainException("Invalid old password");
            }

            if (PasswordHash != newPasswordHash)
            {
                PasswordHash = newPasswordHash;
            }
        }

        public void ResetPassword(string newPasswordHash)
        {
            PasswordHash = newPasswordHash;
        }

        public void UpdateRefreshToken(string token)
        {
            RefreshToken = token;
        }

        public void SetDetails(string email, string phone, string firstName, string lastName)
        {
            Email = email;
            Phone = phone;
            FirstName = firstName;
            LastName = lastName;
        }

        public void SetTeam(int teamId)
        {
            TeamId = teamId;
        }
        public void SetRole(int roleId)
        {
            RoleId = roleId;
        }

        public void SetActivated(bool isActive)
        {
            IsActive = isActive;
        }
        public void SetIsDeleted(bool isDeleted)
        {
            IsDeleted = isDeleted;
        }
        public bool CanCreateUser()
        {
            return Role.Name == RoleName.SuperAdmin;
        }
    }
}
