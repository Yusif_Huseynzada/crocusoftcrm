﻿using CrocuSoftCRM.Domain.AggregatesModel.ProjectAggregate;
using CrocuSoftCRM.Domain.AggregatesModel.RoleAggregate;
using CrocuSoftCRM.SharedKernel.Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Domain.AggregatesModel.UserAggregate
{
    public class UserProject : Entity, IAggregateRoot
    {
        public int UserId { get; private set; }
        public int ProjectId { get; private set; }
        public User User { get; private set; }
        public Project Project { get; private set; }

        public UserProject()
        {
        }

        public UserProject(int userId, int projectId)
        {
            UserId = userId;
            ProjectId = projectId;
        }

        public UserProject(User user, Project project)
        {
            UserId = user.Id;
            ProjectId = project.Id;
            User = user;
            Project = project;
        }

        public void AddToInfo(int userId, int projectId)
        {
            UserId = userId;
            ProjectId = projectId;
        }
    }
}
