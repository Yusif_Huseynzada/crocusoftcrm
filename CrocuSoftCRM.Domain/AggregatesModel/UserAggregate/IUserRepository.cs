﻿using CrocuSoftCRM.Domain.AggregatesModel.DailyReportAggregate;
using CrocuSoftCRM.SharedKernel.Domain.Seedwork;

namespace CrocuSoftCRM.Domain.AggregatesModel.UserAggregate
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
