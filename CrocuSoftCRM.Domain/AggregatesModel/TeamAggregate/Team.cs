﻿using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.SharedKernel.Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CrocuSoftCRM.Domain.AggregatesModel.TeamAggregate
{
    public class Team : Entity, IAggregateRoot
    {
        public string TeamName { get; private set; }

        private readonly List<User> _users;
        public IReadOnlyCollection<User> Users => _users;

        public Team()
        {
            _users = new List<User>();

        }

        public void SetDetails(string teamName)
        {
            TeamName = teamName;
        }
        
    }
}
