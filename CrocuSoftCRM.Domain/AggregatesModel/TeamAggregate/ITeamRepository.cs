﻿using CrocuSoftCRM.Domain.AggregatesModel.UserAggregate;
using CrocuSoftCRM.SharedKernel.Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrocuSoftCRM.Domain.AggregatesModel.TeamAggregate
{
    public interface ITeamRepository : IRepository<Team>
    {
        void Delete(Team team);
    }
}
